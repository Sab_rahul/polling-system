from dataclasses import field
#from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError  
from . models import User

# from poll.models import Poll

class PollForm(forms.Form):
    q_title = forms.CharField(label="Write title of your Question",max_length=20)
    question = forms.CharField(label="Write your Question",max_length=50,widget=forms.Textarea)
    q_choice1 = forms.CharField(label="Option 1",max_length="20")
    q_choice2 = forms.CharField(label="option 2",max_length="20")
    q_choice3 = forms.CharField(max_length="20",label="Option 3")
    q_choice4 = forms.CharField(max_length="20",label="Option 4")
    
    # class Meta:
    #     model=Poll
    #     fields=('question','q_title')

class CustomUserCreationForm(UserCreationForm):  
    username = forms.CharField(label='username', min_length=5, max_length=150)  
    email = forms.EmailField(label='email')  
    password1 = forms.CharField(label='password', widget=forms.PasswordInput)  
    password2 = forms.CharField(label='Confirm password', widget=forms.PasswordInput)  
  
    # def username_clean(self):  
    #     username = self.cleaned_data['username'].lower()  
    #     new = User.objects.filter(username = username)  
    #     if new.count():  
    #         raise ValidationError("User Already Exist")  
    #     return username  
  
    # def email_clean(self):  
    #     email = self.cleaned_data['email'].lower()  
    #     new = User.objects.filter(email=email)  
    #     if new.count():  
    #         raise ValidationError(" Email Already Exist")  
    #     return email  
  
    # def clean_password2(self):  
    #     password1 = self.cleaned_data['password1']  
    #     password2 = self.cleaned_data['password2']  
  
    #     if password1 and password2 and password1 != password2:  
    #         raise ValidationError("Password don't match")  
    #     return password2  
  
class SigninForm(forms.Form):
    username = forms.CharField(label='username', min_length=5, max_length=150)  
    password1 = forms.CharField(label='password', widget=forms.PasswordInput)  
