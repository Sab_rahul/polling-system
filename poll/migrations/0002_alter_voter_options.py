# Generated by Django 3.2.12 on 2022-06-30 14:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('poll', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='voter',
            options={'verbose_name_plural': 'Voters'},
        ),
    ]
