from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Poll(models.Model):
    q_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User,on_delete=models.SET_NULL,null=True)
    question = models.TextField(max_length=50)
    q_title = models.CharField(max_length=20)
    q_choices = models.JSONField(null=True)
    votes = models.JSONField(null=True)
        
    def __str__(self):
        return self.q_title
    class Meta:
        verbose_name_plural = 'polls'

class Voter(models.Model):
    user = models.ForeignKey(User,on_delete=models.SET_NULL,null=True)
    q_id = models.ForeignKey(Poll,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.user)
    class Meta:
        verbose_name_plural = 'Voters'