from django.contrib import admin
from django.urls import path
from . import views
urlpatterns = [
    path("",views.signin,name='signin'),
    path("register",views.register,name='register'),
    path('signout/<user>',views.signout, name='signout'),
    path("index/<user>",views.index, name='index'),
    path("question/<user>",views.question,name='question'),
    path("answer/<user>",views.answer, name='answer'),
    path("delete/<user>/<q_id>",views.delete,name='delete'),
    path("answer_delete/<user>/<q_id>/<key>",views.answer_delete,name='answer_delete'),
    path("poll/<user>",views.poll,name='poll'),
    path("choice/<user>/<q_id>/<key>",views.choice,name='choice'),


]