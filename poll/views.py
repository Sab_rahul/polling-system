from http.client import HTTPResponse
from xml.sax.handler import property_declaration_handler
from django.http import HttpResponse
from django.shortcuts import render,redirect
from . models import User,Poll,Voter
from django.db.models import Q
from django.contrib import messages
from django.core.exceptions import ValidationError  
from django.contrib.auth import login,logout,authenticate
from . forms import PollForm,CustomUserCreationForm, SigninForm

# Create your views here.
def register(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            username=form.cleaned_data['username']
            email=form.cleaned_data['email']
            password1=form.cleaned_data['password1']
            password2=form.cleaned_data['password2']
            
            new = User.objects.filter(username = username)  
            if new.count():  
                raise ValidationError("User Already Exist")  
            
            new = User.objects.filter(email=email)  
            if new.count():  
                raise ValidationError(" Email Already Exist")  

            if password1 and password2 and password1 != password2:  
                raise ValidationError("Password don't match")
            
            obj=User()
            obj.username=username
            obj.email=email
            obj.password=password1
            obj.save()
            id=obj.id
            return redirect('index',user=id)
    else:
        form=CustomUserCreationForm()
    return render(request,'poll/register.html',{'form': form})

def signin(request):
    if request.method == 'POST':
        form = SigninForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            raw_password = form.cleaned_data['password1']
            username=form.cleaned_data['username']
            obj=User.objects.get(username=username)
            if obj and raw_password == obj.password:
                login(request, obj)
            else:
                messages.error(request,f'account does not exist')
                #raise ValidationError('User does not exist or password do not match')
            id=obj.id
            return redirect('index', user=id)
            
           
    else:
        form = SigninForm()
    return render(request, 'poll/signin.html', {'form': form})  





def index(request,user):
    if request.user.is_authenticated:
        data_list=[]
        obj=Poll.objects.all()
        name=User.objects.get(id=user)
        votes=Voter.objects.filter(user=user)
        li=[]
        for vote in votes:
            li.append(vote.q_id)
        print(li)
        for data in obj:
            if data not in li:
                dict_new={}
                data_dict={
                    'q_id': data.q_id,
                    'question': data.question,
                    'choices': data.q_choices,
                    'votes': data.votes,
                    'user': str(data.user),
                    'id':user,
                    'name':name.username
                }
                for i in data_dict['choices']:    
                    count1=0
                    for key,value in data_dict['votes'].items():
                        if value == i:
                            count1+=1
                    dict_new[i]= count1
                data_dict['vote_count']= dict_new                    
                data_list.append(data_dict)
        context={"data":data_list}
        return render(request,"poll/index.html",context)
    else: 
        return redirect('signin')

def question(request,user):
    if request.user.is_authenticated:
        data_list=[]
        user_id=User.objects.get(pk=user)
        obj=Poll.objects.filter(user=user_id)
        for data in obj:
            dict_new={}
            data_dict={
                'q_id': data.q_id,
                'question': data.question,
                'choices': data.q_choices,
                'votes': data.votes,
                'user': str(data.user),
                'id': user,
                'name':user_id.username
            }
            for i in data_dict['choices']:    
                count1=0
                for key,value in data_dict['votes'].items():
                    if value == i:
                        count1+=1
                dict_new[i]= count1
            data_dict['vote_count']= dict_new
                    

            data_list.append(data_dict)
        context={"data":data_list}
        return render(request,"poll/question.html",context)
    else: 
        return redirect('signin')

def answer(request,user):
    if request.user.is_authenticated:
        data_list=[]
        user_id=Voter.objects.filter(user=user).values('q_id')
        name=User.objects.get(id=user)
        ls=[i for i in user_id]
        for i in user_id:
            obj=Poll.objects.filter(q_id=i['q_id'])
            for data in obj:
                dict_new={}
                data_dict={
                    'q_id': data.q_id,
                    'question': data.question,
                    'choices': data.q_choices,
                    'votes': data.votes,
                    'user': str(data.user),
                    'id': user,
                    'name': name.username
                }
                for i in data_dict['choices']:    
                    count1=0
                    for key,value in data_dict['votes'].items():
                        if value == i:
                            count1+=1
                    dict_new[i]= count1
                    data_dict['vote_count']= dict_new


                data_list.append(data_dict)
        context={"data":data_list}
        return render(request,"poll/answer.html",context)
    else: 
        return redirect('signin')

def poll(request,user):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = PollForm(request.POST)
            user=User.objects.get(pk=user)
            if form.is_valid():
                obj=Poll()
                obj.user=user
                obj.question=form.cleaned_data['question']
                obj.q_title=form.cleaned_data['q_title']
                q_choice1=form.cleaned_data['q_choice1']
                q_choice2=form.cleaned_data['q_choice2']
                q_choice3=form.cleaned_data['q_choice3']
                q_choice4=form.cleaned_data['q_choice4'] 
                q_choices=[str(q_choice1),str(q_choice2),str(q_choice3),str(q_choice4)]
            # obj.votes={}
            #print(q_choices)
            #data=json.dumps(q_choices)
            #print(data)
                obj.q_choices=q_choices
                votes={}
            #datav=json.dumps(votes)
                obj.votes=votes       
                obj.save()
                id=user.id
                return redirect('question',user=id)

        else:
            form=PollForm()
        return render(request, 'poll/poll.html',{'form':form})
    else:
        return redirect('signin')

def choice(request,user,q_id,key):
    if request.user.is_authenticated:
        user_id=User.objects.get(id=user)
        question=Poll.objects.get(pk=q_id)
        username=user_id.username
        question.votes[username]=key
        question.save()
        obj=Voter.objects.filter(q_id=question) & Voter.objects.filter(user=user_id)
        for i in obj:
            break
        else:
            obj1=Voter()
            obj1.q_id=question
            obj1.user=user_id
            obj1.save()
        id=user_id.id
        return redirect('answer',user=id)
    else:
        return redirect('signin')

def delete(request,user,q_id):
    if request.user.is_authenticated:
        question=Poll.objects.get(q_id=q_id)
        name=User.objects.get(id=user)
        question.delete()
        id=name.id
        return redirect('question',user=id)
    else: 
        return redirect('signin')

def answer_delete(request,user,q_id,key):
    if request.user.is_authenticated:
        question=Poll.objects.get(q_id=q_id)
        name=User.objects.get(id=user)
        question.votes.pop(key)
        question.save()
        obj=Voter.objects.filter(user=user) & Voter.objects.filter(q_id=q_id)
        obj.delete()
        id=name.id
        return redirect('answer',user=id)
    else:
        return redirect('signin')
    
def signout(request,user):
    logout(request)
    return redirect('/')